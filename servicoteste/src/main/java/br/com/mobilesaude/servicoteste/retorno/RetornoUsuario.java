package br.com.mobilesaude.servicoteste.retorno;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetornoUsuario extends RetornoPadrao {

	@JsonProperty("usuario")
	private Usuario usuario;

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

}